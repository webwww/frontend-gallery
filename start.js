const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs')
const axios = require('axios')
const multer  = require('multer')
const port = 3131


const linkImageUpload = 'link-image-upload'
const dest = 'public/uploads/'

const storageConfig = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, dest)
      },
    filename: function (req, file, cb) {
        cb(null,  file.originalname );
    }
  });

app.use(multer({storage: storageConfig }).array('picture', 12))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.get('/get-pics', function(req, res){
    let linksArray = getSavedPics()
    res.send({resp: linksArray})
})

app.get('/remove-img/:id', function(req, res){
    if (removeElementFromJsonById("database.json", req.params.id)) {
        res.send(true)
    }
})

app.post('/update-picture', function (req, res, next) {


    let addedImgs = [];
    req.files.forEach(elem => {

        let name = elem.filename.split('.')
        let ext = name[name.length - 1]
        console.log(ext)
        if (ext === 'json'){
            console.log(elem)
            let file = fs.readFileSync(elem.destination + elem.filename, 'utf-8')
            let linksArray = JSON.parse(file).galleryImages
            linksArray.forEach(info => {
                let addedId = addLinkToJsonedArray("database.json", info.url)
                addedImgs.push({id: addedId, link: info.url})
            })
            return
        }

        let link = "./uploads/" + elem.filename
        let addedId = addLinkToJsonedArray("database.json", link)
        addedImgs.push({id: addedId, link: link})
    })
    res.send({addedPics: addedImgs})
    return
  })

app.post('/create-by-link', async function(req, res){

    let extensions = ['jpg', 'png', 'json']

    let link = req.body[linkImageUpload]

    let name = link.split('/')
    let ext = name[name.length - 1].split('.')
    ext = ext[ext.length - 1]
    if (!extensions.includes(ext)){ // валидация стоит еще и на фронте
        res.status(400).send({
            message: 'wrong ext'
        });
        return
    }

    if (ext === "json") {
        const { data } = await axios.get(link);
        var linksArray = data.galleryImages.map(elem => {
            return elem.url
        })

        let finishedArray = []
        linksArray.forEach(element => {
            let addedId = addLinkToJsonedArray("database.json", element)
            finishedArray.push({id: addedId, link: element})
        })

        console.log(finishedArray)
        res.send({addedPics: finishedArray})
        return
    }

    let addedId = addLinkToJsonedArray("database.json", link)
    res.send({addedPics:[{id: addedId, link: link}]})
})

function addLinkToJsonedArray(filename, link) {
    let file = fs.readFileSync(filename)
    let json = JSON.parse(file)
    
    let lastId = (json.length === 0) ? 0 : json[json.length - 1].id

    json.push({id: lastId + 1, link: link})
    fs.writeFileSync(filename, JSON.stringify(json, null, 2))
    return lastId + 1
}

function removeElementFromJsonById(filename, id){
    let file = fs.readFileSync(filename)
    let json = JSON.parse(file)
    index = json.findIndex(x => x.id == id);
    json.splice(index, 1);
    fs.writeFileSync(filename, JSON.stringify(json, null, 2))
    return true
}

function getSavedPics(){
    let file = fs.readFileSync('database.json', 'utf-8')
    let linksArray = JSON.parse(file)
    return linksArray
}


app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
  });

