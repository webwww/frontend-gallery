let preloader = {
    turnOn: function(targetClass){
      document.querySelector(".preloader").style.display = "block"; // fake prealoader
      $(targetClass).css('opacity', '0.1'); // fake prealoader
    },

    turnOff: function(targetClass){
      $(targetClass).css('opacity', '1'); // fake prealoader
      document.querySelector(".preloader").style.display = "none"; // fake prealoader
    }
  }
  
  async function getAllPictures(url){
    let response = await fetch(url);
    let links = await response.json()

    var lastLi = document.getElementById("last-li");
    var list = document.getElementById("pic-list");
    links.resp.forEach(elem => {
        appendPictureToGallery(list, lastLi, elem.link, elem.id);
    })
    setTimeout(function() {
      preloader.turnOff('.main-container')
      ;}, 1000);
}

function deleteItem(event){
    let elemId = event.composedPath()[0].id
    let url = '/remove-img/' + elemId.split("-")[1]
    
    $.ajax({
      url: url,         /* Куда пойдет запрос */
      method: 'get',             /* Метод передачи (post или get) */
      dataType: 'html',          /* Тип данных в ответе (xml, json, script, html). */
      data: $(this).serialize(),     /* Параметры передаваемые в запросе. */
      success: function(data){ 
        if (data){
          removePictureById(elemId)
        }
      }
    });
}

function removePictureById(id){
    let removedElement = $(`#${id}`).closest('li')
    $(removedElement).remove()
  }

  function appendPictureToGallery(list, lastLi, link, id){

      if (link === "") return

      var listItem = document.createElement('li');
      var image = document.createElement('img');
      image.src = link;

      var crossItem = document.createElement('span')
      crossItem.className = "img__close"
      crossItem.innerHTML = "&times;"
      crossItem.id = `img-${id}`
      crossItem.onclick = function(event) { deleteItem(event); };

      listItem.appendChild(image);
      list.insertBefore(listItem, lastLi);
      listItem.insertBefore(crossItem, image)
  }
